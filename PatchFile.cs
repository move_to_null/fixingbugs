﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixingBugs
{
    class PatchFile
    {
        static public void patchFile(ObservableCollection<ListLink> listFile)
        {
            string newFile = "";
            string nameFile = "";
            string [] newFileArr = new string [20];
            

            for (int i = 0; i < listFile.Count; i++)
            {
                nameFile = listFile[i].PathFile;
                newFile = string.Empty;

                using (StreamReader reader = new StreamReader(
                      (System.IO.Stream)File.OpenRead(nameFile),
                       System.Text.Encoding.Default, false))
                {
                    newFile = reader.ReadToEnd();
                }

                for (int j = 0; j < listFile[i].ChangeString.Count; j++)
                    newFile = newFile.Replace(
                        listFile[i].ChangeString[j].OriginalString,
                        listFile[i].ChangeString[j].NewString);

                using (StreamWriter file = new System.IO.StreamWriter(
                    nameFile, false, System.Text.Encoding.Default))
                {
                    file.Write(newFile);
                }
            }
        }
    }
}
