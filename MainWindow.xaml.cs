﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace FixingBugs
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Список файлов с правками
        public ObservableCollection<ListLink> listFile;        

        public MainWindow()
        {
            InitializeComponent();            
            listFile = new ObservableCollection<ListLink>();

            textNameFile.Text = Properties.Settings.Default.nameFile;
            if (textNameFile.Text == "" || textNameFile.Text == null)
            {
                textNameFile.Text = System.Windows.Forms.Application.StartupPath + @"\settings.xml";
                Properties.Settings.Default.nameFile = textNameFile.Text;
                Properties.Settings.Default.Save();
            }

            if (File.Exists(textNameFile.Text))
                SerializeList.Deserialize(listFile, textNameFile.Text);
            else
                SerializeList.SerializeObject(listFile, textNameFile.Text);

            listNameFile.ItemsSource = listFile;
            listNameFile.SelectedIndex = 0;     
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            // TODO: предупреждать, что измен-я не сохранены.
        }
        /// <summary>
        /// Выбор нового файла
        /// </summary>
        private void SelectList_Event(object sender, SelectionChangedEventArgs e)
        {
            if (listNameFile.SelectedIndex == -1)
                return;
            tableChanges.ItemsSource = listFile[listNameFile.SelectedIndex].ChangeString;
            tableChanges.SelectedIndex = 0;
        }
        /// <summary>
        /// Выбор новой правки файла
        /// </summary>
        private void SelectTable_Event(object sender, SelectionChangedEventArgs e)
        {
            if (tableChanges.SelectedIndex == -1)
                return;
            originalString.Text = listFile[listNameFile.SelectedIndex].ChangeString[tableChanges.SelectedIndex].OriginalString;
            newString.Text = listFile[listNameFile.SelectedIndex].ChangeString[tableChanges.SelectedIndex].NewString;
        }
        /// <summary>
        /// Добавить новый файл для правки
        /// </summary>
        private void AddFile_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".c";
            dlg.Filter = "File|*.c|Head|*.h|Text|*.txt|Start|*.s";
            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();
            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                ListLink item = new ListLink(dlg.FileName);
                item.ChangeString.Add(new ReplaceString());  
                listFile.Add(item);
            }
        }
        /// <summary>
        /// Удалить файл для правки
        /// </summary>
        private void DelFile_Click(object sender, RoutedEventArgs e)
        {
            if (listNameFile.SelectedIndex == -1)
                return;
            listFile.Remove(listFile[listNameFile.SelectedIndex]);
        }
        /// <summary>
        /// Добавить новую строку правки
        /// </summary>
        private void AddString_Click(object sender, RoutedEventArgs e)
        {
            if (listNameFile.SelectedIndex == -1)
                return;
            ReplaceString str = new ReplaceString();
            listFile[listNameFile.SelectedIndex].ChangeString.Add(str);
        }
        /// <summary>
        /// Удалить строку правки
        /// </summary>
        private void DelString_Click(object sender, RoutedEventArgs e)
        {
            if (listNameFile.SelectedIndex == -1 ||
                tableChanges.SelectedIndex == -1)
                return;
            listFile[listNameFile.SelectedIndex].ChangeString.Remove(listFile[listNameFile.SelectedIndex].ChangeString[tableChanges.SelectedIndex]);
            tableChanges.SelectedIndex = 0;
        }
        /// <summary>
        /// Сохранить строку правки
        /// </summary>
        private void SaveString_Click(object sender, RoutedEventArgs e)
        {
            if (listNameFile.SelectedIndex == -1 ||
                tableChanges.SelectedIndex == -1)
                return;
            listFile[listNameFile.SelectedIndex].ChangeString[tableChanges.SelectedIndex].OriginalString = originalString.Text;
            listFile[listNameFile.SelectedIndex].ChangeString[tableChanges.SelectedIndex].NewString = newString.Text;
        }
        /// <summary>
        /// Сохранить все правки в файлах и применить их
        /// </summary>
        private void Patch_Click(object sender, RoutedEventArgs e)
        {
            SerializeList.SerializeObject(listFile, textNameFile.Text);
            PatchFile.patchFile(listFile);
            System.Windows.Forms.MessageBox.Show("Завершено");
        }
        /// <summary>
        /// Создать новую базу
        /// </summary>
        private void NewBase_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "settings"; // Default file name
            dlg.DefaultExt = ".xml"; // Default file extension
            dlg.Filter = "Base|*.xml"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // сохранить в настройках
                textNameFile.Text = dlg.FileName;
                Properties.Settings.Default.nameFile = dlg.FileName;
                Properties.Settings.Default.Save();

                listFile.Clear();
                SerializeList.SerializeObject(listFile, textNameFile.Text);
            }
        }
        /// <summary>
        /// Открыть базу
        /// </summary>
        private void OpenBase_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".xml";
            dlg.Filter = "Base|*.xml";
            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();
            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // сохранить в настройках
                textNameFile.Text = dlg.FileName;
                Properties.Settings.Default.nameFile = dlg.FileName;
                Properties.Settings.Default.Save();

                listFile.Clear();

                // прочитать базу
                SerializeList.Deserialize(listFile, textNameFile.Text);
            }
        }
        /// <summary>
        /// Сохранить изменения в базе
        /// </summary>
        private void SaveBase_Click(object sender, RoutedEventArgs e)
        {
            SerializeList.SerializeObject(listFile, textNameFile.Text);
        }
    }
}
