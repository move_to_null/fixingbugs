﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Serialization;
using System.IO;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace FixingBugs
{
    /// <summary>
    /// Все изменения в одном файле
    /// </summary>
    [Serializable]
    public class ListLink : INotifyPropertyChanged
    {
        #region Constructors
        public ListLink() : this("")
        {
        }
        public ListLink(string pathFile)
        {
            PathFile = pathFile;
            ChangeString = new ObservableCollection<ReplaceString>();
            //ReplaceString str = new ReplaceString();
            //ChangeString.Add(str);            
        }
        #endregion Constructors        

        #region Properties
        [XmlIgnore]
        public string _pathFile;
        [XmlIgnore]
        public ObservableCollection<ReplaceString> _changeString;
        /// <summary>
        /// Путь к файлу
        /// </summary>
        public string PathFile { get { return _pathFile; } set { _pathFile = value; RaisePropertyChanged("PathFile"); } }
        /// <summary>
        /// Список измен-й
        /// </summary>
        public ObservableCollection<ReplaceString> ChangeString { get { return _changeString; } set { _changeString = value; RaisePropertyChanged("ChangeString"); } }
        #endregion Properties

        #region Обновление интерфейса
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }
        protected void RaisePropertyChanged(string propertyName)
        {
            RaisePropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        #endregion Обновление интерфейса
    }

    /// <summary>
    /// Одно измен-е в файле
    /// </summary>
    [Serializable]
    public class ReplaceString : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [XmlIgnore]
        public string _originalString;        
        [XmlIgnore]
        public string _newString;

        // Исходная строка
        public string OriginalString { get { return _originalString; } set { _originalString = value; RaisePropertyChanged("OriginalString"); } }
        // Новая строка
        public string NewString { get { return _newString; } set { _newString = value; RaisePropertyChanged("NewString"); } }

        protected virtual void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }
        protected void RaisePropertyChanged(string propertyName)
        {
            RaisePropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
    }

    /// <summary>
    /// Класс с методами сериализации xml файлов
    /// </summary>
    public static class SerializeList
    {
        public static void SerializeObject(this ObservableCollection<ListLink> list, string fileName)
        {
            var serializer = new XmlSerializer(typeof(ObservableCollection<ListLink>));
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                serializer.Serialize(stream, list);
            }
        }
        public static void Deserialize(this ObservableCollection<ListLink> list, string fileName)
        {
            List<ListLink> listTmp = new List<ListLink>();
            Deserialize(listTmp, fileName);

            foreach (var item in listTmp)
                list.Add(item);
        }
        public static void SerializeObject(this List<ListLink> list, string fileName)
        {
            var serializer = new XmlSerializer(typeof(List<ListLink>));
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                serializer.Serialize(stream, list);
            }
        }
        public static void Deserialize(this List<ListLink> list, string fileName)
        {
            var serializer = new XmlSerializer(typeof(List<ListLink>));
            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                var other = (List<ListLink>)(serializer.Deserialize(stream));
                list.Clear();
                list.AddRange(other);
            }
        }
    }

}
